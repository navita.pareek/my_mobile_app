import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'components/video_cell.dart';
import 'containers/take_picture.dart';
import 'package:camera/camera.dart';

List<CameraDescription> cameras;

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  cameras = await availableCameras();
  runApp(MaterialApp(
   home: new MyApp()
 ));
}



class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  var _isLoading = true;
  var videos;

  _fetchData() async {
    print("Fetching..");
    final url = "https://api.letsbuildthatapp.com/youtube/home_feed";
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final map = json.decode(response.body);
      final videoJson = map["videos"];
      print(videoJson);
      setState(() {
        _isLoading = false;
        videos = videoJson;
      });
    }
  }

  _openCamera() {
    print("opening camera..");
    final firstCamera = cameras.first;
    Navigator.of(context)
        .push(MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return new TakePictureScreen(camera: firstCamera);
        }));

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
          appBar: new AppBar(
            title: new Text("Welcome Home!!"),
            actions: <Widget>[
              new IconButton(
                  icon: new Icon(Icons.refresh),
                  onPressed: () {
                    print("reloading..");
                    setState(() {
                      _isLoading = true;
                    });
                    _fetchData();
                  }),
              new IconButton(
                  icon: new Icon(Icons.camera),
                  onPressed: () {
                    setState(() {
                      _isLoading = true;
                    });
                    _openCamera();
                  })    
            ],
          ),
          body: new Center(
              child: _isLoading
                  ? new Text("loading")
                  : new ListView.builder(
                      itemCount: this.videos != null ? this.videos.length : 0,
                      itemBuilder: (context, i) {
                        final video = this.videos[i];
                        return new FlatButton(
                            padding: new EdgeInsets.all(0.0),
                            onPressed: () {
                              print("pressed..");
                              Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                      builder: (context) =>
                                          new DetailsPage()));
                            },
                            child: new VideoCell(video));
                      })));
  }
}

class DetailsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Detail Page"),
      ),
      body: new Center(
        child: new Text("it is a new page!!")
      )
    );
  }  
}
